import java.applet.Applet;
import java.awt.*;
import java.awt.event.*;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Time;
import java.text.*;
import java.util.*;
import java.awt.*;

import javax.swing.*;

/**
 * Implements the TSP user interface and  the main loop of the EA (selection and replacement methods)
 *
 * @author Craig Feldman
 * Modified from provided code
 *
 * 12 May 2015
 */
public class TSP {

	// For random number generation
	static Random rand;

    // Size of tournament
    static int tournamentSize = 3;

    // Chromosomes for next generation
    static Chromosome[] newChromosomes;

    // Number of chromosomes to continue unaltered.
    static int numElite;

    // % of population that will be elite
    static double percentElite = 0.1;

    /**
     * How many cities to use.
     */
    protected static int cityCount;

    /**
     * How many chromosomes to use.
     */
    protected static int populationSize;

    /**
     * The part of the population eligable for mateing.
     */
    protected static int matingPopulationSize;

    /**
     * The part of the population selected for mating.
     */
    protected static int selectedParents;

    /**
     * The current generation
     */
    protected static int generation;

    /**
     * The list of cities.
     */
    protected static City[] cities;

    /**
     * The list of chromosomes.
     */
    protected static Chromosome[] chromosomes;

    /**
    * Frame to display cities and paths
    */
    private static JFrame frame;

    /**
     * Integers used for statistical data
     */
    private static double min;
    private static double avg;
    private static double max;
    private static double sum;

    /**
     * Width and Height of City Map, DO NOT CHANGE THESE VALUES!
     */
    private static int width = 600;
    private static int height = 600;


    private static Panel statsArea;
    private static TextArea statsText;


    /* Writing to an output file with the costs. */
    private static void writeLog(String content) {
        String filename = "results.out";
        FileWriter out;

        try {
            out = new FileWriter(filename, true);
            out.write(content + "\n");
            out.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /* Deals with printing same content to System.out and GUI */
    private static void print(boolean guiEnabled, String content) {
        if(guiEnabled) {
            statsText.append(content + "\n");
        }

        System.out.println(content);
    }

	/**
	 * Evolves the current population by selecting numElite elite chromosomes to continue unaltered
	 * the remaining chromosomes are the result of crossover with tournament selection for the parents.
	 *
	 * The current population is then replaced with the new one
	 */
    public static void evolve() {
       //System.out.println("\nEvolving");

        // Add elite chromosomes to next population
        //Chromosome.sortChromosomes(chromosomes, populationSize);
        for (int i = 0; i < numElite; ++i)
            newChromosomes[i] = chromosomes[i];


        Chromosome p1, p2, c1, c2;
        for (int i = numElite; i < populationSize-1; i+=2) {
            p1 = tournamentSelect(tournamentSize);
            p2 = tournamentSelect(tournamentSize);

            c1 = new Chromosome(p1.cityList, p1.cost);
            c2 = new Chromosome(p2.cityList, p2.cost);

            p1.crossOver(p2, c1, c2);

            //System.out.println("Mutating");

            c1.mutate();
            c2.mutate();

            c1.calculateCost(cities);
            c2.calculateCost(cities);

            newChromosomes[i] = c1;
            newChromosomes[i+1] = c2;

           // System.out.println("Created two children");
/*
            System.out.println("0p:" + p1);
            System.out.println("1p:" + p2);
            System.out.println("0c:" + c1);
            System.out.println("1c:" + c2);
            */
        }

        chromosomes = newChromosomes;

        // int[] p1a ={5, 7, 1, 3, 6, 4, 2};
	   // int[] p2a ={4, 6, 2, 7, 3, 1, 5};

        /*
	    Chromosome p1 = chromosomes[0];
	    Chromosome p2 =  chromosomes[1];

	    Chromosome c1 = new Chromosome(p1.cityList, p1.cost);
	    Chromosome c2 = new Chromosome(p2.cityList, p2.cost);
        */



        /*


        */



        //System.out.println("3p:" + Arrays.toString(chromosomes[3].cityList) + chromosomes[3].cost);
	    //System.out.println("4p:" + Arrays.toString(chromosomes[4].cityList) + chromosomes[4].cost);

    }

	/**
	 * Performs tournament selection on the population.
	 * @param size the size of the tournaments.
	 * @return the champion chromosome.
	 */
    private static Chromosome tournamentSelect(int size) {
        ArrayList<Integer> arr = new ArrayList<Integer>();
        int index = rand.nextInt(populationSize);
        arr.add(index);
        Chromosome champion = chromosomes[index];

        for (int i = 0; i < size -1; ++i) {
            index = rand.nextInt(populationSize);
            // Check that it is unique
            while (arr.contains(index))
                index = rand.nextInt(populationSize);

            if (champion.cost > chromosomes[index].cost) {
               // System.out.println("replacing champion " + champion.cost + " with " + chromosomes[index].cost);
                champion = chromosomes[index];
            }
        }

        return champion;
    }

    /** Update the display */
    public static void updateGUI() {
        Image img = frame.createImage(width, height);
        Graphics g = img.getGraphics();
        FontMetrics fm = g.getFontMetrics();

        g.setColor(Color.black);
        g.fillRect(0, 0, width, height);

        if (true && (cities != null)) {
            g.setColor(Color.green);
            for (int i = 0; i < cityCount; i++) {
                int xpos = cities[i].getx();
                int ypos = cities[i].gety();
                g.fillOval(xpos - 5, ypos - 5, 10, 10);
            }

            g.setColor(Color.gray);
            for (int i = 0; i < cityCount; i++) {
                int icity = chromosomes[0].getCity(i);
                if (i != 0) {
                    int last = chromosomes[0].getCity(i - 1);
                    g.drawLine(
                        cities[icity].getx(),
                        cities[icity].gety(),
                        cities[last].getx(),
                        cities[last].gety());
                }
            }
        }
        frame.getGraphics().drawImage(img, 0, 0, frame);
    }


    public static void main(String[] args) {
	    rand = new Random();
        DateFormat df = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
        Date today = Calendar.getInstance().getTime();
        String currentTime  = df.format(today);

        int runs;
        boolean display = false;
        String formatMessage = "Usage: java TSP 100 500 1 [gui] \n java TSP [NumCities] [PopSize] [Runs] [gui]";

        if (args.length < 3) {
            System.out.println("Please enter the arguments");
            System.out.println(formatMessage);
            display = false;
        } else {

            if (args.length > 3) {
                display = true; 
            }

            try {
                cityCount = Integer.parseInt(args[0]);
                populationSize = Integer.parseInt(args[1]);
                numElite = (int)Math.ceil(percentElite * populationSize);
                runs = Integer.parseInt(args[2]);

                if(display) {
                    frame = new JFrame("Traveling Salesman");
                    statsArea = new Panel();

                    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                    frame.pack();
                    frame.setSize(width + 300, height);
                    frame.setResizable(false);
                    frame.setLayout(new BorderLayout());
                    
                    statsText = new TextArea(35, 35);
                    statsText.setEditable(false);

                    statsArea.add(statsText);
                    frame.add(statsArea, BorderLayout.EAST);
                    
                    frame.setVisible(true);
                }


                min = 0;
                avg = 0;
                max = 0;
                sum = 0;

                // create a random list of cities
                // Note: This is outside the run loop so that the multiple runs
                // are tested on the same city set
                cities = new City[cityCount];
                for (int i = 0; i < cityCount; i++) {
                    cities[i] = new City(
                        (int) (Math.random() * (width - 10) + 5),
                        (int) (Math.random() * (height - 50) + 30));
                }

                writeLog("Run Stats for experiment at: " + currentTime);
                for (int y = 1; y <= runs; y++) {
                    print(display,  "Run " + y + "\n");

                    // create the initial population of chromosomes
                    chromosomes = new Chromosome[populationSize];
                    newChromosomes = new Chromosome[populationSize];

                    for (int x = 0; x < populationSize; x++) {
                        chromosomes[x] = new Chromosome(cities);
                        //TODO
                        newChromosomes[x] = new Chromosome(chromosomes[x].cityList, chromosomes[x].cost);
                    }

                    generation = 0;
                    double thisCost = 0.0;

                    while (generation < 100) {
                        evolve();
                        generation++;

                        Chromosome.sortChromosomes(chromosomes, populationSize);
                        double cost = chromosomes[0].getCost();
                        thisCost = cost;

                        NumberFormat nf = NumberFormat.getInstance();
                        nf.setMinimumFractionDigits(2);
                        nf.setMinimumFractionDigits(2);

                        print(display, "R" + y + ":G" + generation + "\tBest: " + (int) thisCost + "\tWorst: " + (int) chromosomes[populationSize - 1].cost);

                        if(display) {
                            updateGUI();
                        }
                    }

                    writeLog(thisCost + "");

                    if (thisCost > max) {
                        max = thisCost;
                    }

                    if (thisCost < min || min == 0) {
                        min = thisCost;
                    }

                    sum +=  thisCost;

                    print(display, "");
                }

                avg = sum / runs;
                print(display, "Statistics after " + runs + " runs.");
                print(display, "Solution found after " + generation + " generations:" + "\n");
                print(display, "MIN: " + min + " AVG: " + avg + " MAX: " + max + "\n");

            } catch (NumberFormatException e) {
                System.out.println("Please ensure you enter integers for cities and population size");
                System.out.println(formatMessage);
            }
        }
    }
}