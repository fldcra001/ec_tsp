import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

/**
 * Implements the EA’s variation methods (genetic operators)
 *
 * @author Craig Feldman
 * Modified from provided code
 *
 * 12 May 2015
 */
class Chromosome {

	// Probability of a mutation occurring
	double mutationRate = 0.5;
	// number of cities
	int cityCount;

	/**
     * The list of cities, which are the genes of this chromosome.
     */
    protected int[] cityList;

    /**
     * The cost of following the cityList order of this chromosome.
     */
    protected double cost;

	/**
	 * Creates a new chromosome.
	 * @param cityList int[] list of cities to visit.
	 * @param cost cost of this list.
	 */
	Chromosome(int[] cityList, double cost) {
		cityCount = cityList.length;
		this.cityList = cityList.clone();
		this.cost = cost;
	}

    /**
     * Creates a new chromosome object.
     * Shuffles the cityList into a random permutation.
     * @param cities The order that this chromosome would visit the cities.
     */
    Chromosome(City[] cities) {
     //   Random generator = new Random();

	    cityCount = cities.length;
        cityList = new int[cities.length];
        //cities are visited based on the order of an integer representation [o,n] of each of the n cities.
        for (int x = 0; x < cities.length; x++)
            cityList[x] = x;

	    // System.out.println("Before:" + Arrays.toString(cityList));

	    //shuffle the order so we have a random initial order
	    // Implementing Fisher–Yates shuffle
	    for (int i = cityList.length - 1; i > 0; i--)   {
		    int index = TSP.rand.nextInt(i + 1);
		    // Simple swap
		    int tmp = cityList[index];
		    cityList[index] = cityList[i];
		    cityList[i] = tmp;
	    }

        calculateCost(cities);
	    //System.out.println("After:" + Arrays.toString(cityList));
    }

	/**
	 * Performs crossover with this chromosome and another parent, producing two offspring.
	 * Makes use of partially matched crossover (PMX) to ensure that TSP constraints aren't violated.
	 *
	 * Example of PMX (crossover between index 2 and 3):
	 * P1 = 5, 7, 1, 3, 6, 4, 2
	 * P2 = 4, 6, 2, 7, 3, 1, 5
	 * C1 = 4, 6, 2, 3, 7, 5, 1
	 * C2 = 5, 7, 1, 6, 3, 2, 5
	 * http://www.ceng.metu.edu.tr/~ucoluk/research/publications/tspnew.pdf
	 *
	 * @param p2 the second parent that will be used for crossover with this chromosome.
	 * @param c1 the first child chromosome.
	 * @param c2 the second child chromosome.
	 */
	public void crossOver(Chromosome p2, Chromosome c1, Chromosome c2){
		Chromosome p1 = this;

		int crossoverPoint = TSP.rand.nextInt(cityList.length -1) + 1;

        //System.out.println("crossing over at " + crossoverPoint);

		// set to parent city list
		c1.cityList = p1.cityList.clone();
		c2.cityList = p2.cityList.clone();

		// http://www.ceng.metu.edu.tr/~ucoluk/research/publications/tspnew.pdf
		// crossover to get 1st child
		for (int i = 0; i <= crossoverPoint; ++i) {
			int index = -1;
			int toSwap = p2.getCity(i);
			// swap c1[i] with occurrence of p2[i] in c1
			//int index = Arrays.asList(c1.cityList).indexOf(toSwap);
			for (int j=0; j < cityCount; ++j) {
				if (c1.cityList[j] == toSwap)
					index = j;
			}

			if (index == -1)
				System.err.println("accessing -1: " + toSwap + Arrays.toString(c1.cityList));
			// swap i and index
			int tmp = c1.getCity(index);
			c1.setCity(index, c1.getCity(i));
			c1.setCity(i, tmp);

			// 2nd child
			toSwap = p1.getCity(i);

			for (int j=0; j < cityCount; ++j) {
				if (c2.cityList[j] == toSwap)
				index = j;
			}

			tmp = c2.getCity(index);
			c2.setCity(index, c2.getCity(i));
			c2.setCity(i, tmp);
		}
	}

    /**
     * Applies mutation to the chromosome
     * This occurs with probability = mutationRate.
     */
    public void mutate() {
	    double rand = Math.random();
	    // Assign bias towards inversion mutations
        if (rand < 0.8*mutationRate) {
            // apply inversion between start and end
            int start = TSP.rand.nextInt(cityCount);
            int end = TSP.rand.nextInt(cityCount);

            // Swap
            if (end < start) {
                int tmp = start;
                end = start;
                start = tmp;
            }
            //System.out.println("Before Swap " + start +":"+end + Arrays.toString(cityList));
            reverse(start, end);
            //System.out.println("After  Swap " + start +":"+end + Arrays.toString(cityList));
        }
	    else if (rand < mutationRate){
	        // swap random cities
	        int r1 = TSP.rand.nextInt(cityList.length);
	        int r2 = TSP.rand.nextInt(cityList.length);
	        int tmp;
	        tmp = cityList[r1];
	        cityList[r1] = cityList[r2];
	        cityList[r2] = tmp;
	        //System.out.println("Swapped " + r1+ ":"+r2);
        }

    }

    /**
     * Reverses a subsection of an array.
     * This method is used for inversion mutation on chromosomes
     *
     * @param start the location where the reversal must start
     * @param end the location where the reversal must end
     */
    private void reverse(int start, int end) {
        while (start < end) {

            // swap
            int tmp;
            tmp = cityList[end];
            cityList[end] = cityList[start];
            cityList[start] = tmp;

            ++start;
            --end;
        }
    }

    /**
     * Calculate the cost of the specified list of cities.
     *
     * @param cities A list of cities.
     */
    void calculateCost(City[] cities) {
        cost = 0;
        for (int i = 0; i < cityList.length - 1; i++) {
            double dist = cities[cityList[i]].proximity(cities[cityList[i + 1]]);
            cost += dist;
        }
    }

    /**
     * Get the cost for this chromosome. This is the amount of distance that
     * must be traveled.
     */
    double getCost() {
        return cost;
    }

    /**
     * @param i The city you want.
     * @return The ith city.
     */
    int getCity(int i) {
        return cityList[i];
    }

    /**
     * Set the order of cities that this chromosome would visit.
     *
     * @param list A list of cities.
     */
    void setCities(int[] list) {
        for (int i = 0; i < cityList.length; i++) {
            cityList[i] = list[i];
        }
    }

    /**
     * Set the index'th city in the city list.
     *
     * @param index The city index to change
     * @param value The city number to place into the index.
     */
    void setCity(int index, int value) {
        cityList[index] = value;
    }

    /**
     * Sort the chromosomes by their cost.
     *
     * @param chromosomes An array of chromosomes to sort.
     * @param num         How much of the chromosome list to sort.
     */
    public static void sortChromosomes(Chromosome chromosomes[], int num) {
        Chromosome ctemp;
        boolean swapped = true;
        while (swapped) {
            swapped = false;
            for (int i = 0; i < num - 1; i++) {
                if (chromosomes[i].getCost() > chromosomes[i + 1].getCost()) {
                    ctemp = chromosomes[i];
                    chromosomes[i] = chromosomes[i + 1];
                    chromosomes[i + 1] = ctemp;
                    swapped = true;
                }
            }
        }
    }

    /**
     * returns chromosome info
     */
    public String toString() {
        return Arrays.toString(cityList) + " C: " + cost;
    }


}
